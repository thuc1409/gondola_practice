export const AUT_URL = `https://www.agoda.com`;

// Store all waiting variables
export const SHORT_CONTROL_WAIT = 5;
export const DYNAMIC_WAIT = 0.5;
export const MED_WAIT = 30;

export const MONTHS = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

// Control Props
export const PROP_INNER_TEXT = "innerText";

// REGEX
export const REGEX_REMOVE_LINE_BREAK_AND_BLANK = /(^\s*(?!.+)\n+)|(\n+\s+(?!.+)$)/g;
export const REGEX_EXPECT_NUMBER = /[^0-9.]/g;

export const DAYS_OF_WEEK = ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT'];

export const REVIEW_CATEGORIES = ["Cleanliness", "Location", "Value for money", "Facilities", "Service"];