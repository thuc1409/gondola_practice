import constant = require("../constant/constant");
import { gondola } from "gondolajs";

export function getDate(value: string, baseDate?: string):string {
    let date = new Date();
    if (baseDate !== undefined) date = new Date(baseDate);
    if (value.indexOf("|", 0) != -1) {
        let valuePart = value.split("|")[0];
        if (value.split("|")[1].toUpperCase() == "DAY") {
            if (valuePart.includes("-", 0)) {
                date.setDate(date.getDate() - parseInt(valuePart))
            }
            else {
                date.setDate(date.getDate() + parseInt(valuePart))
            }
        }
        else if (value.split("|")[1].toUpperCase() == "MONTH") {
            if (valuePart.includes("-", 0)) {
                date.setMonth(date.getMonth() - parseInt(valuePart));
            }
            else {
                date.setMonth(date.getMonth() + parseInt(valuePart));
            }
        }
    }
    else if (value.toUpperCase() == "<TODAY>") {
        // Implement later
    }

    // This case for select next day of weeks
    else if (value.toUpperCase().match(/<NEXT [a-zA-z]{3}>/g) != null) {
        let vDay = constant.DAYS_OF_WEEK.indexOf(value.substring(6, 9).toUpperCase())
        let cDay = date.getDay();
        let plusValue!:number;
        if (vDay != -1) {
            if(cDay < vDay) plusValue = vDay - cDay
            else if (cDay >= vDay) plusValue = 7 - (cDay - vDay);
        }
        date.setDate(date.getDate() + plusValue);
    }
    else {
        date = new Date(value)
    }
    return formatDateTime(date);
}

export function formatDateTime(value: string|Date, format: string = "MM/DD/YY") {
    let returnDate: string = "";
    let date = new Date(value);
    let y = date.getFullYear();
    let m = date.getMonth() + 1;
    let d = date.getDate();
    switch (format) {
        case "DD/MM/YY":
            returnDate = `${("0" + d).slice(-2)}/${("0" + m).slice(-2)}/${y}`
            break;
        case "MM/DD/YY":
            returnDate = `${("0" + m).slice(-2)}/${("0" + d).slice(-2)}/${y}`
            break;
        default:
            break;
    }

    return returnDate
}
export async function selectDatePicker(value: string):Promise<void> {
    let date = new Date(getDate(value));
    let targetYear = date.getFullYear();
    let targetMonth = constant.MONTHS[date.getMonth()];
    let targetDay = date.getDate();

    let mainXPath = `(//div[@class='Popup__content']/div[@class='DayPicker']//div[@class='DayPicker-Month'])[1]`;

    let currentMonthYear = await gondola.getControlProperty(`${mainXPath}/div[@class='DayPicker-Caption']/div`, constant.PROP_INNER_TEXT);
    /**
     * Select Month (later)
     */

    // Select Day
    await gondola.click(`${mainXPath}/div[@class='DayPicker-Body']//div[starts-with(@class,'DayPicker-Day') and not(contains(@class,'--disable')) ]/span[text()='${targetDay}']`);
}

export function getCurrentMonth(digitNumber: number = 2):string {
    let date = new Date()
    return ("0" + (date.getMonth() + 1)).slice(-digitNumber)
}

export function getCurrentYear():string {
    let date = new Date()
    return date.getFullYear().toString();
}

export async function checkTextContain(element: string | ILocator, value: string):Promise<void> {
    let text = await gondola.getControlProperty(element, "innerText");

    if (text.indexOf(value, 0) != -1) {
        gondola.checkEqual(true, true,`${text} has contains ${value}`);
    }
    else {
        gondola.checkEqual(true, false, `${text} has not contains ${value}`);
    }
}

export async function getInnerTextProp(locator: ILocator | string):Promise<any> {
    return await gondola.getControlProperty(locator, "innerText");
}

export async function getTitleProp(locator: ILocator | string):Promise<any> {
    return await gondola.getControlProperty(locator, "title");
}