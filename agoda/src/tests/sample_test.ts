import { gondola, TestCase, TestModule } from "gondolajs";
import homePage from "../pages/homePage";
import Constant = require("../constant/constant");
import searchResultPage from "../pages/searchResultPage";
import { SearchFilter } from "../data_objects/searchfilter";
import hotelPage from "../pages/hotelPage";
import { Hotel } from "../data_objects/hotel";

var hotelObj = new Hotel("", "Da Nang", "<NEXT FRI>", "+3|day", "Family", 2, 4);

TestModule("Simple Test");

TestCase("TC01: Search and sort hotel successfully", async () => {
  let filterObj = new SearchFilter();
  filterObj.lowestPrice = true;
  filterObj.verifyItems = 5;

  await homePage.navigateTo();
  await homePage.searchHotels(hotelObj);
  await searchResultPage.checkHotelsPlace(hotelObj.place, 5);

  await searchResultPage.applyFilters(filterObj);
  await searchResultPage.checkResultsDisplayByLowestPrice(filterObj);
  await searchResultPage.checkHotelsPlace(hotelObj.place, 5);
});

TestCase("TC02: Search and filter hotel successfully", async () => {
  let filterObj = new SearchFilter();
  filterObj.minPrice = 500000;
  filterObj.maxPrice = 1000000;
  filterObj.rating = 3;
  filterObj.verifyItems = 5;

  await homePage.navigateTo();
  await homePage.searchHotels(hotelObj);
  await searchResultPage.checkHotelsPlace(hotelObj.place, 5);

  await searchResultPage.applyFilters(filterObj);
  await searchResultPage.checkHotelsPlace(hotelObj.place, 5);
  await searchResultPage.checkResultsDisplayByStar(filterObj);
  await searchResultPage.checkResultsDisplayByPrice(filterObj);

  await searchResultPage.clearPriceRangeFilter();
});

TestCase("TC03: Add hotel into Favorite successfully", async () => {
  let filterObj = new SearchFilter();
  filterObj.propertyFacilities = "Non-smoking";

  await homePage.navigateTo();
  await homePage.searchHotels(hotelObj);
  await searchResultPage.checkHotelsPlace(hotelObj.place, 5);

  await searchResultPage.applyFilters(filterObj);
  await searchResultPage.verifyReviewPointTooltip(5, Constant.REVIEW_CATEGORIES);

  let hotelNo5Obj = await searchResultPage.getInfoOfChosenHotel(5);
  hotelNo5Obj.facilities = "Swimming pool";
  await searchResultPage.chooseHotel(5);
  await hotelPage.checkHotelInfo(hotelNo5Obj);

  await gondola.closeCurrentTab();
  let hotelNo1Obj = await searchResultPage.getInfoOfChosenHotel(1);
  hotelNo1Obj.facilities = "Non-smoking";
  await searchResultPage.chooseHotel(1);
  await hotelPage.checkHotelInfo(hotelNo1Obj);
});
