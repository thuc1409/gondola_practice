import { getDate, formatDateTime } from "../helper/utilityHelper";

export class Hotel {

    private _name: string = '';
    private _place: string = '';
    private _checkInDate: string = '';
    private _checkOutDate: string = '';
    private _travelType: string = '';
    private _rooms: string | number = '';
    private _adults: string | number = '';
    private _facilities: any;
    private _reviewPoint: string | number = '';

    constructor(name: string = "", place: string = "", checkInDate: string = "", checkOutDate: string = "",
        travelType: string = "", rooms: string | number = "", adults: string | number = "", facilities: any = "",
        reviewPoint: string = "") {
        this._name = name;
        this._place = place;
        this._checkInDate = checkInDate;
        this._checkOutDate = checkOutDate;
        this._travelType = travelType;
        this._rooms = rooms;
        this._adults = adults;
        this._facilities = facilities;
        this._reviewPoint = reviewPoint;
    }

    get name(): string {
        return this._name
    }
    set name(value: string) {
        this._name = value;
    }

    get place(): string {
        return this._place
    }
    set place(value: string) {
        this._place = value;
    }

    get facilities(): any {
        return this._facilities
    }
    set facilities(value: any) {
        if (!Array.isArray(value)) value = [value];
        this._facilities = value;
    }

    get reviewPoint(): string | number {
        return this._reviewPoint
    }
    set reviewPoint(value: string | number) {
        this._reviewPoint = value;
    }

    get checkInDate(): string {
        return getDate(this._checkInDate);
    }
    set checkInDate(value: string) {
        this._checkInDate = value;
    }

    get checkOutDate(): string {
        let checkInDate = getDate(this._checkInDate);
        return getDate(this._checkOutDate, formatDateTime(checkInDate));
    }
    set checkOutDate(value: string) {
        this._checkOutDate = value;
    }

    get travelType(): string {
        return this._travelType
    }
    set travelType(value: string) {
        this._travelType = value;
    }

    get rooms(): string | number {
        return this._rooms
    }
    set rooms(value: string | number) {
        this._rooms = value;
    }

    get adults(): string | number {
        return this._adults
    }
    set adults(value: string | number) {
        this._adults = value;
    }
}