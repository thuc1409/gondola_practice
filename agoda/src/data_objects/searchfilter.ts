export class SearchFilter {
  private _lowestPrice: boolean = false;
  private _rating!: number;
  private _minPrice!: number;
  private _maxPrice!: number;
  private _propertyFacilities: any;
  private _verifyItems!: number;
  private _verifyItemNo!: number;

  get lowestPrice(): boolean {
    return this._lowestPrice;
  }
  set lowestPrice(value: boolean) {
    this._lowestPrice = value;
  }

  get rating(): number {
    return this._rating;
  }
  set rating(value: number) {
    this._rating = value;
  }

  get minPrice(): number {
    return this._minPrice;
  }
  set minPrice(value: number) {
    this._minPrice = value;
  }

  get maxPrice(): number {
    return this._maxPrice;
  }
  set maxPrice(value: number) {
    this._maxPrice = value;
  }

  get propertyFacilities(): any {
    return this._propertyFacilities;
  }
  set propertyFacilities(value: any) {
    if (!Array.isArray(value)) value = [value];
    this._propertyFacilities = value;
  }

  get verifyItems(): number {
    return this._verifyItems;
  }
  set verifyItems(value: number) {
    this._verifyItems = value;
  }

  get verifyItemNo(): number {
    return this._verifyItemNo;
  }
  set verifyItemNo(value: number) {
    this._verifyItemNo = value;
  }
}

export default new SearchFilter();
