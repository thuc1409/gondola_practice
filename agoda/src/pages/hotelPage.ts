import { Hotel } from "../data_objects/hotel";
import { gondola } from "gondolajs";
import utilityHelper = require("../helper/utilityHelper");

export class HotelPage {
    private lblHotelName: ILocator = { xpath: `//h1[@data-selenium='hotel-header-name']` };
    private lblHotelScore: ILocator = { xpath: `//div[@class='ReviewPlate']//span[@data-selenium='hotel-header-review-score']` };
    private lblPopularFacility(value: string): ILocator {
        return { xpath: `//div[contains(@class,'PopularFacilities')]//span[@data-element-name='popular-property-feature']//span[contains(text(),'${value}')]` };
    }

    public async checkHotelInfo(hotel: Hotel): Promise<void> {
        let actual = await utilityHelper.getInnerTextProp(this.lblHotelName);
        await gondola.checkEqual(actual, hotel.name);

        actual = await utilityHelper.getInnerTextProp(this.lblHotelScore);
        await gondola.checkEqual(actual, hotel.reviewPoint);
        for (let i in hotel.facilities) {
            await gondola.checkControlExist(this.lblPopularFacility(hotel.facilities[i]));
        }
    }
}

export default new HotelPage();