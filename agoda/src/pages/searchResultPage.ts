import { gondola, KeyCode } from "gondolajs";
import constant = require("../constant/constant");
import { SearchFilter } from "../data_objects/searchfilter";
import { isNullOrEmpty } from "../helper/objectHelper";
import { Hotel } from "../data_objects/hotel";
import utilityHelper = require("../helper/utilityHelper");

export class SearchResultPage {

    /*======================== LOCATORS ========================*/
    private hotelItems: ILocator = { xpath: `//ol[@class='hotel-list-container']//li[@data-selenium='hotel-item']` };
    private lblHotelName = (i: number): ILocator => {
        return { xpath: `(${this.hotelItems}//div[@class='InfoBox']//h3[@data-selenium='hotel-name'])[${i}]` };
    }
    private lblAreaCity = (i: number): ILocator => {
        return { xpath: `(${this.hotelItems}//div[@class='InfoBox']//span[@class='areacity-name-text'])[${i}]` };
    }
    private lblStar = (i: number): ILocator => {
        return { xpath: `(${this.hotelItems}//div[@class='InfoBox']//span[@class='star-rating display-inline']/i[@data-selenium='hotel-star-rating'])[${i}]` };
    }
    private lblPrice = (i: number): ILocator => {
        return { xpath: `(${this.hotelItems}//div[@class='price-box']//span[@class='price-box__price__amount'])[${i}]` };
    }
    private lblReviewPoint = (i: number): ILocator => {
        return { xpath: `(${this.hotelItems}//div[@class='ReviewSection']//span[@class='ReviewScore-Number'])[${i}]` };
    }
    private btnSortByLowestPrice: ILocator = { xpath: `//div[@id='sort-bar']//a[@data-element-name='search-sort-price']` };
    private btnStars: ILocator = { xpath: `//div[@data-element-value='StarRating']/button` };
    private btnPriceRange: ILocator = { xpath: `//div[@data-element-value='PriceFilterRange']/button` };
    private btnMore: ILocator = { xpath: `//div[@data-element-value='more']/button` };
    private bubblePillDropdown: ILocator = { xpath: `//div[starts-with(@class,'PillDropdown__Bubble')]` };
    private chkRatingStar(star: number): ILocator {
        return { xpath: `${this.bubblePillDropdown}//span[@data-element-name='search-filter-starrating' and @data-element-value='${star}']//input[@type='checkbox']` };
    }
    private chkPropertyFacility(prop: string): ILocator {
        return { xpath: `${this.bubblePillDropdown}//span[@data-component='search-filter-hotelfacilities']//span[@class='filter-item-content' and text()='${prop}']/ancestor::span//input[@type='checkbox']` };
    }
    private startDotSlice: ILocator = { xpath: `${this.bubblePillDropdown}///div[@class='rc-slider-handle rc-slider-handle-1' and contains(@style,'left: 0%')]` };
    private endDotSlice: ILocator = { xpath: `${this.bubblePillDropdown}///div[@class='rc-slider-handle rc-slider-handle-2' and contains(@style,'left: 100%')]` };
    private txtMinPrice: ILocator = { xpath: `${this.bubblePillDropdown}//input[@id='price_box_0']` };
    private txtMaxPrice: ILocator = { xpath: `${this.bubblePillDropdown}//input[@id='price_box_1']` };
    private btnClearPrice: ILocator = { xpath: `${this.bubblePillDropdown}//div[@id='PriceFilterRange']//a[@class='filter-btn clear-filter-box']` };
    private lblReviewCategoryName(name: string): ILocator {
        return { xpath: `${this.hotelItems}//following::div[@class='demographics-review-container']//span[@data-selenium='review-name' and text()='${name}']` };
    }
    private lblReviewCategoryPoint(point: string): ILocator {
        return { xpath: `${this.hotelItems}//following::div[@class='demographics-review-container']//strong[@data-selenium='review-point' and text()='${point}']` };
    }

    /*======================== FUNCTIONS ========================*/
    public async checkHotelsPlace(placeName: string, amountItems: number): Promise<void> {
        for (let i = 1; i <= amountItems; i++) {
            await utilityHelper.checkTextContain(this.lblAreaCity(i), placeName);
        }
    }

    public async checkResultsDisplayByLowestPrice(filterObj: SearchFilter): Promise<void> {
        let prices = [];
        for (let i = 1; i <= filterObj.verifyItems; i++) {
            let price: string = await utilityHelper.getInnerTextProp(this.lblPrice(i));
            prices.push(Number(price.replace(constant.REGEX_EXPECT_NUMBER, "")));
        }

        for (let i = 0; i < prices.length - 1; i++) {
            if (prices[i] < prices[i + 1]) continue;
            else {
                await gondola.checkEqual(true, false, `Hotel No.${i} Price are not sorted right ordering`);
            }
        }
        await gondola.checkEqual(true, true, `First ${filterObj.verifyItems} hotel are sorted by lowest price.`);
    }

    public async checkResultsDisplayByStar(filterObj: SearchFilter): Promise<void> {
        let stars = [];
        for (let i = 1; i <= filterObj.verifyItems; i++) {
            let star: string = await utilityHelper.getTitleProp(this.lblStar(i));
            stars.push(Number(star.substring(0, star.length - 5)));
        }

        for (let i = 0; i < stars.length; i++) {
            if (filterObj.rating <= stars[i] && stars[i] < (filterObj.rating + 1)) continue;
            else {
                await gondola.checkEqual(true, false, `Hotel No.${i} Star are not sorted right filter`);
            }
        }
        await gondola.checkEqual(true, true, `First ${filterObj.verifyItems} hotel are sorted by ${filterObj.rating} star.`);
    }

    public async checkResultsDisplayByPrice(filterObj: SearchFilter): Promise<void> {
        let prices = [];
        for (let i = 1; i <= filterObj.verifyItems; i++) {
            let price: string = await utilityHelper.getInnerTextProp(this.lblPrice(i));
            prices.push(Number(price.replace(constant.REGEX_EXPECT_NUMBER, "")));
        }

        for (let i = 0; i < prices.length; i++) {
            if (filterObj.minPrice <= prices[i] && prices[i] <= filterObj.maxPrice) continue;
            else {
                await gondola.checkEqual(true, false, `Hotel No.${i} Price are not sorted right filter`);
            }
        }
        await gondola.checkEqual(true, true, `First ${filterObj.verifyItems} hotel are sorted by right price range (${filterObj.minPrice} - ${filterObj.maxPrice}).`);
    }

    public async applyFilters(filterObj: SearchFilter): Promise<void> {

        if (filterObj.lowestPrice) {
            await gondola.click(this.btnSortByLowestPrice);
            await gondola.waitForElement(this.hotelItems);
        }

        if (!isNullOrEmpty(filterObj.rating)) {
            // Open
            await gondola.click(this.btnStars);
            await gondola.waitForElement(this.bubblePillDropdown);
            await gondola.setState(this.chkRatingStar(filterObj.rating), true);
            await gondola.waitForElement(this.hotelItems);
            // Close
            await gondola.click(this.btnStars);
        }

        if (!isNullOrEmpty(filterObj.minPrice) || !isNullOrEmpty(filterObj.minPrice)) {
            // Open
            await gondola.click(this.btnPriceRange);
            await gondola.waitForElement(this.bubblePillDropdown);
            await gondola.enter(this.txtMinPrice, filterObj.minPrice.toString());
            await gondola.enter(this.txtMaxPrice, filterObj.maxPrice.toString());
            await gondola.pressKey(KeyCode.Enter);
            // Close
            await gondola.click(this.btnPriceRange);
        }

        if (!isNullOrEmpty(filterObj.propertyFacilities)) {
            await gondola.click(this.btnMore);
            if (!Array.isArray(filterObj.propertyFacilities)) filterObj.propertyFacilities = [filterObj.propertyFacilities];
            for (let i in filterObj.propertyFacilities) {
                await gondola.setState(this.chkPropertyFacility(filterObj.propertyFacilities[i]), true);
            }
            await gondola.click(this.btnMore);
        }
    }

    public async clearPriceRangeFilter(verify = true): Promise<void> {
        await gondola.click(this.btnPriceRange);
        await gondola.waitForElement(this.bubblePillDropdown);
        await gondola.click(this.btnClearPrice);
        if (verify) {

            if (await gondola.doesControlExist(this.startDotSlice)
                && await gondola.doesControlExist(this.endDotSlice)) {
                await gondola.checkEqual(true, true, "The price slice has been reset");
            }
            else await gondola.checkEqual(true, false, "The price slice has not been reset");
        }
        await gondola.click(this.btnPriceRange);
    }

    public async getInfoOfChosenHotel(noOfHotel: number): Promise<Hotel> {
        let hotelObj = new Hotel();
        hotelObj.name = await utilityHelper.getInnerTextProp(this.lblHotelName(noOfHotel));
        hotelObj.place = await utilityHelper.getInnerTextProp(this.lblAreaCity(noOfHotel));
        hotelObj.reviewPoint = await utilityHelper.getInnerTextProp(this.lblReviewPoint(noOfHotel));
        return hotelObj;
    }

    public async verifyReviewPointTooltip(noOfHotel: number, listOfCategory: string[]): Promise<void> {
        gondola.moveMouse(this.lblReviewPoint(noOfHotel));
        for (let i in listOfCategory) {
            await gondola.checkControlExist(this.lblReviewCategoryName(listOfCategory[i]));
        }
    }

    public async chooseHotel(noOfHotel: number): Promise<void> {
        await gondola.click(this.lblHotelName(noOfHotel));
    }
}

export default new SearchResultPage