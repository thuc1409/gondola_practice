import { Hotel } from '../data_objects/hotel';
import { gondola, KeyCode } from 'gondolajs';
import constant = require("../constant/constant");
import utilityHelper = require("../helper/utilityHelper");
import { isNullOrEmpty } from '../helper/objectHelper';
import { isNull } from 'util';

export class HomePage {

    private txtSearchPlace: ILocator = { xpath: `//input[starts-with(@class,'SearchBoxTextEditor')]` };
    private btnSearch: ILocator = { xpath: `//button[@data-selenium='searchButton']` };
    private btnOccupancy: ILocator = { xpath: `//div[@data-selenium='occupancyBox']` };
    private popupDatePicker: ILocator = { xpath: `//div[@class='Popup__content']/div[@class='DayPicker']` };

    private popupTravelerType: ILocator = { xpath: `//div[@class='SegmentOccupancy__travelType']` };
    private lblSoloTraveller: ILocator = { xpath: `${this.popupTravelerType}//div[@data-selenium='traveler-solo']` };
    private lblCouplePair: ILocator = { xpath: `${this.popupTravelerType}//div[@data-selenium='traveler-couples']` };
    private lblFamilyTravelers: ILocator = { xpath: `${this.popupTravelerType}//div[@data-selenium='traveler-families']` };
    private lblGroupTravelers: ILocator = { xpath: `${this.popupTravelerType}//div[@data-selenium='traveler-group']` };
    private lblBusinessTravelers: ILocator = { xpath: `${this.popupTravelerType}//div[@data-selenium='traveler-business']` };

    private popupOccupancy: ILocator = { xpath: `//div[@class='SegmentOccupancy__occupancy']` };
    private occupancyRooms: ILocator = { xpath: `${this.popupOccupancy}//div[@data-selenium='occupancyRooms']` };
    private lblRoomsValue: ILocator = { xpath: `${this.occupancyRooms}/span[@data-selenium='desktop-occ-room-value']` };
    private lblRoomsMinus: ILocator = { xpath: `${this.occupancyRooms}/span[@data-selenium='minus']` };
    private lblRoomsPlus: ILocator = { xpath: `${this.occupancyRooms}/span[@data-selenium='plus']` };
    private occupancyAdults: ILocator = { xpath: `${this.popupOccupancy}//div[@data-selenium='occupancyAdults']` };
    private lblAdultsValue: ILocator = { xpath: `${this.occupancyAdults}/span[@data-selenium='desktop-occ-adult-value']` };
    private lblAdultsMinus: ILocator = { xpath: `${this.occupancyAdults}/span[@data-selenium='minus']` };
    private lblAdultsPlus: ILocator = { xpath: `${this.occupancyAdults}/span[@data-selenium='plus']` };

    public async navigateTo(): Promise<void> {
        gondola.navigate(constant.AUT_URL);
        gondola.waitForElement(this.btnSearch, constant.MED_WAIT);
    }

    public async searchHotels(hotel: Hotel): Promise<void> {

        gondola.enter(this.txtSearchPlace, hotel.place);
        await gondola.pressKey(KeyCode.Enter);
        gondola.waitForElement(this.popupDatePicker);
        await utilityHelper.selectDatePicker(hotel.checkInDate);
        await utilityHelper.selectDatePicker(hotel.checkOutDate);

        gondola.waitForElement(this.popupTravelerType);
        if (!isNullOrEmpty(hotel.travelType)) {
            switch (hotel.travelType.toUpperCase()) {
                case "SOLO":
                    gondola.click(this.lblSoloTraveller);
                    break;
                case "PAIR":
                    gondola.click(this.lblCouplePair);
                    break
                case "FAMILY":
                    gondola.click(this.lblFamilyTravelers);
                    gondola.waitForElement(this.popupOccupancy);
                    await this.selectOccupancyAmount(hotel);
                    break
                // The rest will be implement
                default:
                    break;
            }
        }
        gondola.click(this.btnSearch);
    }

    private async selectOccupancyAmount(hotel: Hotel): Promise<void> {
        let currRooms = await utilityHelper.getInnerTextProp(this.lblRoomsValue);
        let currAdults = await utilityHelper.getInnerTextProp(this.lblAdultsValue);

        // Select Rooms
        while (currRooms != hotel.rooms) {
            if (currRooms > hotel.rooms) gondola.click(this.lblRoomsMinus);
            else gondola.click(this.lblRoomsPlus)

            let temp = await utilityHelper.getInnerTextProp(this.lblRoomsValue)
            if (temp == currRooms || isNull(temp)) {
                gondola.report("There error happened when selecting data");
                break;
            }
            else currRooms = temp;
        }

        // Select Adults
        while (currAdults != hotel.adults) {
            if (currAdults > hotel.adults) gondola.click(this.lblAdultsMinus);
            else gondola.click(this.lblAdultsPlus);

            let temp = await utilityHelper.getInnerTextProp(this.lblAdultsValue)
            if (temp == currAdults || isNull(temp)) {
                gondola.report("There error happened when selecting data");
                break;
            }
            else currAdults = temp;


        }
        gondola.click(this.btnOccupancy);
    }
}

export default new HomePage();