import {TestCase, TestModule, gondola} from "gondolajs";
import loginPage from "../pages/loginPage";
import mainPage from "../pages/mainPage";
import composePage from "../pages/composePage";
import { EMail } from "../data_objects/email";
import { generateRandomNumber, Encode } from "../helper/utilityHelper";

TestModule("LG-Email");

TestCase("TC01: Compose and save draft email successfully", async () => {
    let emailObj = new EMail();
    emailObj.receiver = "thuc.duy.nguyen@logigear.com";
    emailObj.content = "This is test email";
    emailObj.attachment = "attachment.txt";
    emailObj.subject = `This is test email with attachment_${generateRandomNumber(3)}`;

    await loginPage.navigateTo();
    await loginPage.login();
    await mainPage.goToCompose();
    await composePage.compose_Email(emailObj, false);
    await mainPage.checkDraftItem(emailObj);
});

TestCase("TC02: Compose and send email successfully", async () => {
    let emailObj = new EMail();
    emailObj.receiver = "thuc.duy.nguyen@logigear.com";
    emailObj.content = "This is test email";
    emailObj.image = "lg-logo.png";
    emailObj.subject = `This is test email with attachment_${generateRandomNumber(3)}`;

    await loginPage.navigateTo();
    await loginPage.login();
    await mainPage.goToCompose();
    await composePage.compose_Email(emailObj);
    await mainPage.checkInboxItem(emailObj);
    await mainPage.downloadContentMail();
});