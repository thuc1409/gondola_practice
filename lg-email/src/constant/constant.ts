import { Decode } from "../helper/utilityHelper";

export const AUT_URL = `https://sgmail.logigear.com`;
export const USERNAME = `thuc.duy.nguyen`;
export const PASSWORD = Decode("QEJldGExOTk0");
export const STORE_PATH = `\\src\\store\\`;
export const DOWNLOAD_PATH = `\\src\\store\\download`;

// Store all waiting variables
export const SHORT_CONTROL_WAIT = 5;
export const DYNAMIC_WAIT = 0.5;
export const MED_WAIT = 30;

export const MONTHS = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
export const DAYS_OF_WEEK = ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT'];