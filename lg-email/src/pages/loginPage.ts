import { gondola } from "gondolajs";
import constant = require("../constant/constant");

export class LoginPage {
    private txtUsername: ILocator = { xpath: `//input[@id='username']` };
    private txtPassword: ILocator = { xpath: `//input[@id='password']` };
    private btnLogin: ILocator = {
        xpath: `//table[@id='tblMid']//input[@class='btn']`,
    };

    public async navigateTo() {
        await gondola.navigate(constant.AUT_URL);
    }

    public async login() {
        await gondola.enter(this.txtUsername, constant.USERNAME);
        await gondola.enter(this.txtPassword, constant.PASSWORD);
        await gondola.click(this.btnLogin);
    }
}

export default new LoginPage();