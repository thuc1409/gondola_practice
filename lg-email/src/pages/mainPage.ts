import { gondola } from "gondolajs";
import { EMail } from "../data_objects/email";
import utilityHelper = require("../helper/utilityHelper");
import { isNullOrEmpty } from "../helper/objectHelper";
import constant = require("../constant/constant");
import fs from "fs";

export class MainPage {
    private btnNew: ILocator = { xpath: `//a[@id='newmsgc']/span` };
    private btnDrafts: ILocator = { xpath: `//div[@id='mailtree']//span[@fldrnm='Drafts']` };
    private btnInbox: ILocator = { xpath: `//div[@id='mailtree']//span[@fldrnm='Inbox']` };

    private mainContent: ILocator = { xpath: `//div[@class='mainView' and not(@style='display: none;')]` };
    private emailItem(subjectName: string): ILocator {
        return { xpath: `${this.mainContent.xpath}//div[@id='vr']/div[@id='divSubject' and text()='${subjectName}']` };
    }

    private lblMailSubject: ILocator = { xpath: `${this.mainContent.xpath}//div[@id='divConvTopic']` };
    private lblMailTo: ILocator = { xpath: `${this.mainContent.xpath}//div[@id='divTo']//span` };
    private lblMailAttachment: ILocator = { xpath: `${this.mainContent.xpath}//div[@id='divAtch']//a` };
    private lblMailContent: ILocator = { xpath: `${this.mainContent.xpath}//div[@id='divBdy']//span` };
    private lblMailContentWithImage: ILocator = { xpath: `${this.mainContent.xpath}//div[@id='divBdy']//span/img` };

    public async goToCompose():Promise<void> {
        await gondola.click(this.btnNew);
    }

    public async checkDraftItem(email: EMail):Promise<void> {
        await gondola.click(this.btnDrafts);
        await gondola.waitForClickable(this.emailItem(email.subject));

        await this.checkItem(email);
    }

    public async checkInboxItem(email: EMail):Promise<void> {
        await gondola.click(this.btnInbox);
        await gondola.waitForClickable(this.emailItem(email.subject), constant.MED_WAIT);

        await this.checkItem(email);
    }

    public async downloadContentMail(verify = true):Promise<void> {
        let url = await gondola.getControlProperty(this.lblMailContentWithImage, "src");
        let path = `${global.codecept_dir}${constant.DOWNLOAD_PATH}\\logo.png`
        await utilityHelper.downloadImage(url, path);
        if (verify) {
            if (fs.existsSync(path)) await gondola.report(`${path} existed`);
            else gondola.report(`Download Failed`);
        }
    }

    private async checkItem(email: EMail):Promise<void> {
        await gondola.click(this.emailItem(email.subject));

        let actual = await gondola.getText(this.lblMailSubject);
        await gondola.checkEqual(actual, email.subject);

        actual = await gondola.getText(this.lblMailTo);
        await gondola.checkEqual(actual, email.receiver);

        if (!isNullOrEmpty(email.attachment)) {
            await utilityHelper.checkTextContain(this.lblMailAttachment, email.attachment);
        }

        if (!isNullOrEmpty(email.content)) {
            actual = await gondola.getText(this.lblMailContent);
            await gondola.checkEqual(actual, email.content);
        }

        if (!isNullOrEmpty(email.image)) {
            await gondola.checkControlExist(this.lblMailContentWithImage);
        }
    }
}

export default new MainPage();