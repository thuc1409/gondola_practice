import { gondola } from "gondolajs";
import { EMail } from "../data_objects/email";
import constant = require("../constant/constant");
import { isNullOrEmpty } from "../helper/objectHelper";

export class ComposePage {
    private btnSend: ILocator = { xpath: `//a[@id='send']/span` };
    private btnSave: ILocator = { xpath: `//a[@id='save']` };
    private btnAttachFile: ILocator = { xpath: `//a[@id='attachfile']` };
    private btnInsertImage: ILocator = { xpath: `//a[@id='insertimage']` };
    private btnCheckNames: ILocator = { xpath: `//a[@id='checknames']` };

    private txtTo: ILocator = { xpath: `//div[@id='divTo']` };
    private txtCc: ILocator = { xpath: `//div[@id='divCc']` };
    private txtSubject: ILocator = { xpath: `//input[@id='txtSubj']` };
    private txtContent: ILocator = { xpath: `//div[@id='divBdy']` };
    private txtAttached: ILocator = { xpath: `//div[@id='divAtch']//a[@id='lnkAtmt']` };

    private iFrameParent: ILocator = { xpath: `//iframe[@id='iFrameModalDlg']` };
    private iFrameChild: ILocator = { xpath: `//iframe[@class='wh100']` };
    private btnChooseFile: ILocator = { xpath: `//input[@id='file1']` };
    private btnAttach: ILocator = { xpath: `//button[@id='btnAttch']` };

    public async compose_Email(email: EMail, send = true): Promise<void> {
        let handles = await gondola.getAllWindowHandles();
        await gondola.switchToWindow(handles[1]);
        await this.setFields(email);
        if (send) {
            await gondola.click(this.btnSend);
        }
        else {
            await gondola.click(this.btnSave);
            // Close Compose windows
            await gondola.closeCurrentTab();
        }
        await gondola.switchToWindow(handles[0]);
    }

    public async setFields(email: EMail):Promise<void> {

        let attach = async (button: ILocator, name: string) => {
            await gondola.click(button);
            await gondola.switchFrame(this.iFrameParent);
            await gondola.switchFrame(this.iFrameChild);
            await gondola.attachFile(this.btnChooseFile, `${constant.STORE_PATH}${name}`);
            await gondola.click(this.btnAttach);
            // Switch back to parent
            await gondola.switchFrame();
        }

        await gondola.enter(this.txtTo, email.receiver);
        await gondola.click(this.btnCheckNames);
        email.receiver = await gondola.getText(`${this.txtTo.xpath}/span/span`);
        await gondola.enter(this.txtSubject, email.subject);

        await gondola.click(this.txtContent);
        await gondola.pressKey(email.content);

        if (!isNullOrEmpty(email.attachment)) await attach(this.btnAttachFile, email.attachment);

        if (!isNullOrEmpty(email.image)) await attach(this.btnInsertImage, email.image);
    }
}

export default new ComposePage();