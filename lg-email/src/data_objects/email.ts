export class EMail {

    private _receiver: string = '';
    private _subject: string = '';
    private _attachment: string = '';
    private _content: string = '';
    private _image: string = '';

    get receiver(): string {
        return this._receiver
    }
    set receiver(value: string) {
        this._receiver = value;
    }

    get subject(): string {
        return this._subject
    }
    set subject(value: string) {
        this._subject = value;
    }

    get attachment(): string {
        return this._attachment
    }
    set attachment(value: string) {
        this._attachment = value;
    }

    get content(): string {
        return this._content
    }
    set content(value: string) {
        this._content = value;
    }

    get image(): string {
        return this._image
    }
    set image(value: string) {
        this._image = value;
    }
}

export default new EMail();