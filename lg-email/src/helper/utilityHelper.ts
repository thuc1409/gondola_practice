import constant = require("../constant/constant");
import { gondola } from "gondolajs";
const Fs = require('fs')
const Axios = require('axios')


export function getDate(value: string, baseDate?: string): string {
    let date = new Date();
    if (baseDate !== undefined) date = new Date(baseDate);
    if (value.indexOf("|", 0) != -1) {
        let valuePart = value.split("|")[0];
        if (value.split("|")[1].toUpperCase() == "DAY") {
            if (valuePart.includes("-", 0)) {
                date.setDate(date.getDate() - parseInt(valuePart))
            }
            else {
                date.setDate(date.getDate() + parseInt(valuePart))
            }
        }
        else if (value.split("|")[1].toUpperCase() == "MONTH") {
            if (valuePart.includes("-", 0)) {
                date.setMonth(date.getMonth() - parseInt(valuePart));
            }
            else {
                date.setMonth(date.getMonth() + parseInt(valuePart));
            }
        }
    }
    else if (value.toUpperCase() == "<TODAY>") {
        // Implement later
    }

    // This case for select next day of weeks
    else if (value.toUpperCase().match(/<NEXT [a-zA-z]{3}>/g) != null) {
        let vDay = constant.DAYS_OF_WEEK.indexOf(value.substring(6, 9).toUpperCase())
        let cDay = date.getDay();
        let plusValue!: number;
        if (vDay != -1) {
            if (cDay < vDay) plusValue = vDay - cDay
            else if (cDay >= vDay) plusValue = 7 - (cDay - vDay);
        }
        date.setDate(date.getDate() + plusValue);
    }
    else {
        date = new Date(value)
    }
    return formatDateTime(date);
}
export function formatDateTime(value: string | Date, format: string = "MM/DD/YY"): string {
    let returnDate: string = "";
    let date = new Date(value);
    let y = date.getFullYear();
    let m = date.getMonth() + 1;
    let d = date.getDate();
    switch (format) {
        case "DD/MM/YY":
            returnDate = `${("0" + d).slice(-2)}/${("0" + m).slice(-2)}/${y}`
            break;
        case "MM/DD/YY":
            returnDate = `${("0" + m).slice(-2)}/${("0" + d).slice(-2)}/${y}`
            break;
        default:
            break;
    }

    return returnDate;
}

export async function checkTextContain(element: string | ILocator, value: string): Promise<void> {
    let text = await gondola.getControlProperty(element, "innerText");

    if (text.indexOf(value, 0) != -1) {
        gondola.checkEqual(true, true, `${text} has contains ${value}`);
    }
    else {
        gondola.checkEqual(true, false, `${text} has not contains ${value}`);
    }
}

export function generateRandomNumber(digit: number = 2):string {
    let startI = "1", endI = "9";
    for (let i = 0; i < digit; i++) {
        startI = startI + "0";
        endI = endI + "0";
    }
    return Math.floor(Number(startI) + Math.random() * Number(endI)).toString();
}

export async function getInnerTextProp(locator: ILocator | string):Promise<any> {
    return await gondola.getControlProperty(locator, "innerText");
}

/* ============================================================
  Function: Download Image
============================================================ */
export async function downloadImage(url: string, path: string) {
    const writer = Fs.createWriteStream(path)

    const response = await Axios({
        url,
        method: 'GET',
        responseType: 'stream'
    })

    response.data.pipe(writer)

    return new Promise((resolve, reject) => {
        writer.on('finish', resolve)
        writer.on('error', reject)
    })
}

export function Encode(str: string) {
    let buff = new Buffer(str);
    return buff.toString('base64');
}

export function Decode(str: string) {
    let buff = new Buffer(str, 'base64');
    return buff.toString('ascii');
}
