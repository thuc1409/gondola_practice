import { TestCase, TestModule, gondola } from "gondolajs";
import { Constants } from "../common/constants";
import loginPage from "../pages/loginPage";
import menuPage from "../pages/menuPage";
import {TableHelper} from "../helpers/tableHelpers";
TestModule("Simple Test");

Before(async () => {
    await gondola.navigate(Constants.AUT_URL);
    await loginPage.login();
})

TestCase("1st simple test case", async () => {
    await menuPage.navigateToSysTemUser();
    await menuPage.selectMenu("PIM/Add Employee");
    
});


