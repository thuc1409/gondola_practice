export class Employee {
    private _firstName: string = "";
    private _lastName: string = "";

	constructor(firstName: string, lastName:string) {
        this._firstName = firstName;
        this._lastName = lastName;
	}
    
    /**
     * Getter firstName
     * @return {string }
     */
	public get firstName(): string  {
		return this._firstName;
	}

    /**
     * Getter lastName
     * @return {string }
     */
	public get lastName(): string  {
		return this._lastName;
	}

    /**
     * Setter firstName
     * @param {string } value
     */
	public set firstName(value: string ) {
		this._firstName = value;
	}

    /**
     * Setter lastName
     * @param {string } value
     */
	public set lastName(value: string ) {
		this._lastName = value;
	}
}