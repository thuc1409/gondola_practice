import { locator, gondola } from "gondolajs";

class MainMenu {
    @locator private lblFirstLevel(mainMenu: string): ILocator {
        return { xpath: `//ul[@id='mainMenuFirstLevelUnorderedList']/li//*[text()='${mainMenu}']` };
    }

    @locator private lblSubLevel(menu: string): ILocator {
        return { xpath: `//ul/li/a[text()='${menu}']` };
    }

    public async selectMenu(menuPath: string, delimiter: string = "/"): Promise<void> {
        let listMenu: string[] = menuPath.split(delimiter);
        if (listMenu.length == 0) return;

        let lastElement: ILocator = this.lblFirstLevel(listMenu[0])
        await gondola.moveMouse(lastElement, { x: 10, y: 10 });

        for (let i = 1; i < listMenu.length; i++) {
            lastElement = this.lblSubLevel(listMenu[i]);
            await gondola.moveMouse(lastElement, { x: 10, y: 10 });
        }

        await gondola.click(lastElement);
    }

    public async navigateToSysTemUser(): Promise<void> {
        return this.selectMenu("Admin/User Management/Users")
    }
}

export default new MainMenu();