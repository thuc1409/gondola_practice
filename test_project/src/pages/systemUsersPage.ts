import { locator } from "gondolajs";

export class SystemUserPage {
    @locator private txtUserName: ILocator = { xpath: `//input[@id='searchSystemUser_userName']` }
    @locator private ddlUserRole: ILocator = { xpath: `//select[@id='searchSystemUser_userType']` }
    @locator private txtEmployeeName: ILocator = { xpath: `//input[@id='searchSystemUser_employeeName_empName']` }
    @locator private ddlStatus: ILocator = { xpath: `//select[@id='searchSystemUser_status']` }
    @locator private btnSearch: ILocator = { xpath: `//input[@id='searchBtn']` }
    @locator private btnReset: ILocator = { xpath: `//input[@id='resetBtn']` }
    @locator private btnAdd: ILocator = { xpath: `//input[@id='btnAdd']` }
    @locator private btnDelete: ILocator = { xpath: `//input[@id='btnDelete']` }
}
