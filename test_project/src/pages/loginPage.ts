import { locator, gondola } from "gondolajs";
import { Constants } from "../common/constants";

export class LoginPage {

    @locator private txtUsername: ILocator = { xpath: `//input[@id='txtUsername']` };
    @locator private txtPassword: ILocator = { xpath: `//input[@id='txtPassword']` };
    @locator private btnLogin: ILocator = { xpath: `//input[@id='btnLogin']` };

    public async login() {
        await gondola.enter(this.txtUsername, Constants.USERNAME);
        await gondola.enter(this.txtPassword, Constants.PASSWORD);
        await gondola.click(this.btnLogin);
    }
}

export default new LoginPage();