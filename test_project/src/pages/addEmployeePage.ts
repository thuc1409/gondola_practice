import { locator } from "gondolajs"

export class AddEmployee {
    @locator private txtFirstName: ILocator = { xpath: `//input[@id='firstName']` }
    @locator private txtLastName: ILocator = { xpath: `//input[@id='lastName']` }
    @locator private btnSave: ILocator = { xpath: `//input[@id='btnSave']` }
}