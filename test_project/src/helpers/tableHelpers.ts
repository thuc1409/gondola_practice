import { gondola } from "gondolajs";

export namespace TableHelper {
    export async function isTableHeaderExist(table: string): Promise<boolean> {
        return await gondola.doesControlExist(table + "/thead");
    }

    export async function checkItemTableExist(table: string, headerName: string, itemName: string): Promise<void> {
        let items = [];
        let rows = await gondola.getElementCount(`${table}//tbody/tr`);

        if (!isTableHeaderExist(table)) {
            await gondola.report("Header table cannot be null.");
        }

        for (let i = 1; i <= rows; i++) {
            let temp: string = await gondola.getText(`${table}//tbody/tr[${i}]/td[count(${table}}//thead/tr/th[.='${headerName}']/preceding-sibling::th)+1]`);
            items.push(temp);
        }

        let item = items.filter(item => item = itemName);

        if (item.length > 0) {
            await gondola.checkEqual(true, true, `Item '${itemName}' exists on the table`);
        }
        else {
            await gondola.checkEqual(true, false, `Item '${itemName}' does not exist on the table`);
        }
    }
}

