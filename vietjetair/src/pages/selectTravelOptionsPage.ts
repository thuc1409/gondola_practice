import { locator, gondola } from "gondolajs";
import Constant = require("..//constant/constant");
import { Search } from "../data_objects/search";
import { Ticket } from "../data_objects/ticket";
import { isNullOrEmpty } from "../helper/objectHelper";
import utilityHelper = require("../helper/utilityHelper");
import { Flight } from "../data_objects/flight";

enum TYPE {
    DEP = "Dep",
    RET = "Ret"
}

export class SelectTravelOptionsPage {
    private TITLE: string = "Select Travel Options";
    private departSelectedNo: any;
    private returnSelectedNo: any;
    private departFlights: any;
    private returnFlights: any;
    private headerIndex = {
        flightDate: this.getHeaderIndex("Flight Date"),
        departs: this.getHeaderIndex("Departs"),
        arrives: this.getHeaderIndex("Arriver"),
        flightDetail: this.getHeaderIndex("Flight Detail"),
        promo: this.getHeaderIndex("Promo"),
        eco: this.getHeaderIndex("Eco"),
        skyboss: this.getHeaderIndex("Skyboss")
    }

    /*=======================LOCATORS=======================*/
    @locator private lblPageTitle: ILocator = { xpath: `//div[@id='contentwsb']/h1[1]` }
    @locator private lblAdultNo(noOfPassenger: string | number): ILocator {
        return { xpath: `//table[@id='tblPaxCountsInfo']//span[contains(text(),'Adults')]/parent::td[contains(text(),'${noOfPassenger}')]` }
    }
    @locator private btnContinue: ILocator = { xpath: `//a[contains(@href,"button.value='continue'")]` }

    @locator private eleFlightTableCols: ILocator = { xpath: `//div[@id='toDepDiv']//tr[@id='TrvOptGridHdr']/td` };
    @locator private eleFlightTableHeader = (index: number): ILocator => {
        return { xpath: `${this.eleFlightTableCols.xpath}/b[${index}]` };
    }

    @locator private eleFlightTableRows = (type: TYPE): ILocator => {
        return { xpath: `//div[@id='to${type}Div']//table[@class='FlightsGrid']//tr[starts-with(@id,'gridTravelOpt${type}')]` };
    }

    @locator private eleFlightOption = (rowNo: any, colNo: any, type: TYPE): ILocator => {
        return { xpath: `(${this.eleFlightTableRows(type).xpath}[${rowNo}]//tr/td)[${colNo}])` };
    }

    @locator private radFlightPrice = (rowNo: any, colNo: any, type: TYPE): ILocator => {
        return { xpath: `${this.eleFlightOption(rowNo, colNo, type).xpath}/input[@id='gridTravelOptDep']` };
    }

    @locator private lblFlightSoldOut = (rowNo: any, colNo: any, type: TYPE) => {
        return { xpath: `${this.eleFlightOption(rowNo, colNo, type)}/div[@class='ErrorCaption' and text()='This flight has been sold out']` }
    }


    /*=======================FUNCTIONS=======================*/
    public isReturnFlight(search: Search): boolean {
        return (search.flightType.toUpperCase() == "RETURN");
    }

    public async checkPageIsDisplayed(): Promise<void> {
        await gondola.checkControlExist(this.lblPageTitle);
        await gondola.checkText(this.lblPageTitle, this.TITLE);
    }

    public async checkFlightsInformation(search: Search): Promise<void> {
        if (isNullOrEmpty(this.departFlights)) this.getFlightOptions(search);
        let check: Function = (arr: any[], currentIsDepart: boolean) => {
            for (let index = 0; index < arr.length; index++) {
                let actual: string = arr[index].flightDate;
                if (currentIsDepart) gondola.checkEqual(actual.substring(0, actual.length - 4), utilityHelper.formatDateTime(search.departDate, "DD/MM/YY"));
                else gondola.checkEqual(actual.substring(0, actual.length - 4), utilityHelper.formatDateTime(search.returnDate, "DD/MM/YY"), "Verify Flight Date");

                actual = arr[index].departs;
                if (currentIsDepart) gondola.checkEqual(actual.substring(10, actual.length), search.origin.substring(0, search.origin.length - 6));
                else gondola.checkEqual(actual.substring(10, actual.length), search.destination.substring(0, search.destination.length - 6), "Verify Departs");

                actual = arr[index].arrives;
                if (currentIsDepart) gondola.checkEqual(actual.substring(10, actual.length), search.destination.substring(0, search.destination.length - 6), "Verify Arrives");
                else gondola.checkEqual(actual.substring(10, actual.length), search.origin.substring(0, search.origin.length - 6));

                actual = arr[index].promo[index];
                if (actual != "Sold Out") gondola.checkEqual(actual.substring(actual.length - 3, actual.length), search.currency, "Verify Currency");

                actual = arr[index].eco[index];
                if (actual != "Sold Out") gondola.checkEqual(actual.substring(actual.length - 3, actual.length), search.currency, "Verify Currency");

                actual = arr[index].skyboss[index];
                if (actual != "Sold Out") gondola.checkEqual(actual.substring(actual.length - 3, actual.length), search.currency, "Verify Currency");
            }
        }
        check(this.departFlights, true);
        if (isNullOrEmpty(this.returnFlights)) check(this.returnFlights, false);

        // Check Flight Adults
        await gondola.checkControlExist(this.lblAdultNo(search.adults));
    }

    public async selectCheapestFlights(search: Search): Promise<Ticket> {
        let minIndex = (arr: any[], keyName:string) => {
            arr = priceArray(arr, keyName);
            let newArr = arr.map((item) => {
                if (item == "Sold Out") return item;
                else Number(item.replace(Constant.REGEX_EXPECT_NUMBER, ""))
            });
            return newArr.indexOf(Math.min(...newArr));
        }

        let priceArray = (arr: any[], keyName: string) => {
            return arr.map(prop => {
                return prop[keyName]
            });
        }

        let selectMin = async (flightsArray:any[], type:TYPE) => {
            let index;
            if ((index = minIndex(flightsArray, "promo")) !== -1) {
                await gondola.click(this.radFlightPrice(index + 1, this.headerIndex.eco, type))
            }
            else if ((index = minIndex(flightsArray, "eco")) !== -1) {
                await gondola.click(this.radFlightPrice(index + 1, this.headerIndex.promo, type))
            }
            else if ((index = minIndex(flightsArray, "skyboss")) !== -1) {
                await gondola.click(this.radFlightPrice(index + 1, this.headerIndex.skyboss, type))
            }

            if( type == TYPE.DEP) this.departSelectedNo = index;
            else this.returnSelectedNo = index;
        }

        // Select Cheapest Price for Return
        if (!isNullOrEmpty(this.returnFlights)) {
            await selectMin(this.returnFlights, TYPE.RET);
        }

        // Select Cheapest Price for Depart
        await selectMin(this.departFlights, TYPE.DEP);

        return this.parseValueToTicket(search);
    }

    public async parseValueToTicket(search: Search): Promise<Ticket> {

        if (isNullOrEmpty(this.departFlights)) this.getFlightOptions(search);
        let ticketObj = new Ticket();

        ticketObj.departFrom = this.departFlights[this.departSelectedNo].departs.substring(10, this.departFlights.departs[this.departSelectedNo].length)
        ticketObj.departTo = this.departFlights.arrives[this.departSelectedNo].substring(10, this.departFlights.arrives[this.departSelectedNo].length)
        ticketObj.departDate = this.departFlights[this.departSelectedNo].flightDate

        ticketObj.returnFrom = this.returnFlights[this.returnSelectedNo].departs.substring(10, this.returnFlights[this.departSelectedNo].departs.length)
        ticketObj.returnTo = this.returnFlights[this.returnSelectedNo].arrives.substring(10, this.returnFlights[this.departSelectedNo].arrives.length)
        ticketObj.returnDate = this.returnFlights[this.returnSelectedNo].flightDate

        ticketObj.adults = search.adults;
        return ticketObj;
    }

    public async clickContinue(): Promise<void> {
        await gondola.click(this.btnContinue);
    }

    private async getFlightOptions(search: Search): Promise<void> {
        let arr = new Array();

        let getFlight = async (type: TYPE) => {
            let rows = await gondola.getElementCount(this.eleFlightTableRows(type));

            for (let index = 1; index <= rows; index++) {
                let flight = new Flight();
                // Add Flight Date
                flight.flightDate = await utilityHelper.getInnerTextProp(this.eleFlightOption(index, this.headerIndex.flightDate, type));

                // Add Departs
                flight.depart = await utilityHelper.getInnerTextProp(this.eleFlightOption(index, this.headerIndex.departs, type));

                // Add Arrives
                flight.arrive = await utilityHelper.getInnerTextProp(this.eleFlightOption(index, this.headerIndex.arrives, type));

                // Add Flight Detail
                flight.flightDetail = await utilityHelper.getInnerTextProp(this.eleFlightOption(index, this.headerIndex.flightDetail, type));

                // Handle case this flight was sold out
                if (await gondola.doesControlExist(this.lblFlightSoldOut(index, this.headerIndex.promo, type))) {
                    continue;
                }

                // Add Promo
                flight.promo = await utilityHelper.getInnerTextProp(this.eleFlightOption(index, this.headerIndex.promo, type));

                // Add Eco
                flight.eco = await utilityHelper.getInnerTextProp(this.eleFlightOption(index, this.headerIndex.eco, type));

                // Add Skyboss
                flight.skyboss = await utilityHelper.getInnerTextProp(this.eleFlightOption(index, this.headerIndex.skyboss, type));

                // Push flight obj
                arr.push(flight);
            }
            return arr;
        }
        this.departFlights = getFlight(TYPE.DEP);
        if (this.isReturnFlight(search)) this.returnFlights = getFlight(TYPE.RET);
    }

    private async getHeaderIndex(headerName: string): Promise<number> {
        let cols = await gondola.getElementCount(this.eleFlightTableCols);
        let index = 0
        for (index = 0; index < cols; index++) {
            let value = await utilityHelper.getInnerTextProp(this.eleFlightTableHeader(index + 1));
            if (value == headerName) break;
        }
        return index;
    }
}

export default new SelectTravelOptionsPage();