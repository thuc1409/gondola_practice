import { gondola } from "gondolajs";
import constants = require("../constant/constant")
import utilityHelper = require("../helper/utilityHelper");

enum TYPE {
    DEP = "Dep", RET = "Ret"
}

export class SelectFarePage {
    private TITLE = "Select Fare";

    /*=======================LOCATORS=======================*/
    private lblPageTitle: ILocator = { xpath: `//div[@id='contentwsb']/h1[1]` };
    private btnContinue: ILocator = { xpath: `//a[contains(@href,"button.value='continue'")]` };
    private existTickets(type: TYPE): ILocator {
        return { xpath: `(//table[@id='ctrValueViewer${type}Grid']//tr/td[not(@class='vvDayNoFlight') and not(@class='vvDayBlank') and not(@class='vvDaySearchNoFlight')])` };
    }
    private eleTicket(type: TYPE, ticketDate: string): ILocator {
        return { xpath: `${this.existTickets(type).xpath}/div[text()='${ticketDate}']` };
    }
    private eleTicketDay(type: TYPE, index: number): ILocator {
        return { xpath: `${this.existTickets(type).xpath}[${index}]/div` };
    }
    private eleTicketPrice(type: TYPE, index: number): ILocator {
        return { xpath: `${this.existTickets(type).xpath}[${index}]/div/p` };
    }
    private btnNextMonth(type: TYPE): ILocator {
        return { xpath: `//table[@id='ctrValueViewer${type}Grid']//th[@class='vvNext']/a` };
    }
    private btnPrevMonth(type: TYPE): ILocator {
        return { xpath: `//table[@id='ctrValueViewer${type}Grid']//th[@class='vvPrev']/a` };
    }

    /*=======================FUNCTIONS=======================*/
    public async checkPageIsDisplayed(): Promise<void> {
        await gondola.checkControlExist(this.lblPageTitle);
        await gondola.checkText(this.lblPageTitle, this.TITLE);
    }

    public async clickContinue(): Promise<void> {
        await gondola.click(this.btnContinue);
    }

    public async selectCheapestTicketInMonths(monthRange: number): Promise<object> {
        let pricesDep = [];
        let daysDep = [];
        let pricesRet = [];
        let daysRet = [];
        if (this.isReturnFlight()) {
            for (let index = 0; index <= monthRange; index++) {
                let cheapTicket = await this.getCheapestTicket(TYPE.RET)
                pricesRet.push(cheapTicket.price);
                daysRet.push(cheapTicket.day);
                if (index != monthRange) await gondola.click(this.btnNextMonth(TYPE.RET));
                gondola.wait(constants.DYNAMIC_WAIT);
            }
        }
        for (let index = 0; index <= monthRange; index++) {
            let cheapTicket = await this.getCheapestTicket(TYPE.DEP)
            pricesDep.push(cheapTicket.price);
            daysDep.push(cheapTicket.day);
            if (index != monthRange) await gondola.click(this.btnNextMonth(TYPE.DEP));
            gondola.wait(constants.DYNAMIC_WAIT);
        }

        let convertPriceToNum = (arr: any[]) => {
            let newArr: any[] = [];
            for (let i = 0; i < arr.length; i++) {
                let element: string = arr[i];
                newArr.push(Number(element.replace(constants.REGEX_EXPECT_NUMBER, "")));
            }
            return newArr;
        }

        // Convert price to number to calculate lowest price
        let pricesDepInNum = convertPriceToNum(pricesDep);
        let pricesRetInNum = convertPriceToNum(pricesRet);
        let lowestPrice = 999999999;
        let iDep, iRet = 0;
        for (iDep = 0; iDep < pricesDepInNum.length; iDep++) {
            for (iRet = 0; iRet < pricesRetInNum.length; iRet++) {
                if (iRet < iDep) continue;
                if ((iDep + iRet) < lowestPrice) lowestPrice = iDep + iRet;
                else continue;
            }
        }

        // Select Depart
        while ((monthRange - iDep) >= 0) {
            await gondola.click(this.btnPrevMonth(TYPE.DEP));
        }
        await gondola.click(this.eleTicket(TYPE.DEP, daysDep[iDep]));

        if (this.isReturnFlight()) {
            // Select Return
            while ((monthRange - iRet) >= 0) {
                await gondola.click(this.btnPrevMonth(TYPE.RET));
            }
            await gondola.click(this.eleTicket(TYPE.RET, daysRet[iRet]));
        }

        let returnObj = {
            rDayDep: `${daysDep[iDep]}/${utilityHelper.getCurrentMonth()}/${utilityHelper.getCurrentYear()}`,
            rDayRet: `${daysDep[iRet]}/${utilityHelper.getCurrentMonth()}/${utilityHelper.getCurrentYear()}`
        }
        return returnObj;
    }

    public async getCheapestTicket(type: TYPE): Promise<any> {
        let cheapTicket = {
            day: '',
            price: ''
        }
        let obj = await this.getAllFares(type);

        let index = this.getMinPriceIndex(obj.prices);
        cheapTicket.day = obj.days[index];
        cheapTicket.price = obj.prices[index];

        return cheapTicket;
    }

    public async getAllFares(type: TYPE): Promise<any> {
        let obj = {
            days: [] = new Array,
            prices: [] = new Array,
        }

        let availableTickets = await gondola.getElementCount(this.existTickets(type));
        for (let index = 1; index <= availableTickets; index++) {

            let day = await utilityHelper.getInnerTextProp(this.eleTicketDay(type, index));
            let price = await utilityHelper.getInnerTextProp(this.eleTicketPrice(type, index));

            obj.days.push(day);
            obj.prices.push(price);
        }
        return obj;
    }

    private getMinPriceIndex(arr: any[]): number {
        if (arr.length == 0) return -1;
        let newArr: any[] = [];
        for (let i = 0; i < arr.length; i++) {
            let element: string = arr[i];
            newArr.push(Number(element.replace(constants.REGEX_EXPECT_NUMBER, "")));
        }
        let minValue = Math.min(...newArr);

        return newArr.indexOf(minValue);
    }

    private async isReturnFlight(): Promise<boolean> {
        if (await gondola.doesControlExist(this.existTickets(TYPE.RET))) return true;
        else return false;
    }
}

export default new SelectFarePage()