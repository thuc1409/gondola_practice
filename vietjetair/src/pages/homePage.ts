import { gondola } from 'gondolajs';
import { selectDateOfWidget } from '../helper/utilityHelper'
import constants = require("../constant/constant");
import { isNullOrEmpty } from '../helper/objectHelper';
import { Search } from '../data_objects/search';

export class HomePage {

    /*=======================LOCATORS=======================*/
    private rdoReturn: ILocator = { xpath: `//input[@id='ctl00_UcRightV31_RbRoundTrip']` };
    private rdoOneWay: ILocator = { xpath: `//input[@id='ctl00_UcRightV31_RbOneWay']` };
    private ddlOrigin: ILocator = { xpath: `//select[@id='selectOrigin']` };
    private btnDepartDate: ILocator = { xpath: `//input[@id='ctl00_UcRightV31_TxtDepartDate']` };
    private ddlDestination: ILocator = { xpath: `//select[@id='selectDestination']` };
    private btnReturnDate: ILocator = { xpath: `//input[@id='ctl00_UcRightV31_TxtReturnDate']` };
    private btnSearchFlights: ILocator = { xpath: `//input[@id='ctl00_UcRightV31_Btticket']` };
    private chkFindLowestFair: ILocator = { xpath: `//input[@id='ctl00_UcRightV31_ChkInfare']` };
    private imgAutLogo: ILocator = { xpath: `//img[@alt='logo']` };
    private btnAdults: ILocator = { xpath: `//button[@id='ctl00_UcRightV31_CbbAdults_Button']` };
    private lblNumberOfAdult(v: string): ILocator {
        return { xpath: `//ul[@id='ctl00_UcRightV31_CbbAdults_OptionList']/li[.='${v}']` };
    }

    /*=======================FUNCTIONS=======================*/
    public async navigateTo():Promise<void> {
        await gondola.navigate(constants.AUT_URL);
        await gondola.waitForElement(this.imgAutLogo, constants.MED_WAIT);
    }

    public async searchFlights(search: Search):Promise<void> {
        switch (search.flightType.toUpperCase()) {
            case "RETURN":
                gondola.setState(this.rdoReturn, true);
                break;
            case "ONE WAY":
                gondola.setState(this.rdoOneWay, true);
                break;
            default:
                break;
        }

        await gondola.select(this.ddlOrigin, search.origin);
        if (!isNullOrEmpty(search.departDate)) {
            await gondola.click(this.btnDepartDate);
            await selectDateOfWidget(search.departDate);
        }

        await gondola.select(this.ddlDestination, search.destination);
        if (!isNullOrEmpty(search.returnDate)) {
            await gondola.click(this.btnReturnDate);
            await selectDateOfWidget(search.returnDate);
        }

        if (!isNullOrEmpty(search.adults)) {
            await gondola.waitForClickable(this.btnAdults);
            await gondola.click(this.btnAdults);
            await gondola.click(this.lblNumberOfAdult(search.adults));
        }

        if( search.findLowest == true) await gondola.click(this.chkFindLowestFair);
        await gondola.click(this.btnSearchFlights);
    }
}

export default new HomePage();