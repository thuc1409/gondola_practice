import { locator, gondola } from "gondolajs";
import { Ticket } from "../data_objects/ticket";
import utilityHelper = require("../helper/utilityHelper");
import { checkTextContain } from "../helper/utilityHelper";

export class BookingSummaryPage {

    /*=======================LOCATORS=======================*/
    @locator private lblDepartFrom: ILocator = { xpath: `//table[@id='tblLeg1APs']//td[1]` };
    @locator private lblDepartTo: ILocator = { xpath: `//table[@id='tblLeg1APs']//td[2]` };
    @locator private lblDepartDate: ILocator = { xpath: `//table[@id='tblLeg1Info']//span[@id='Leg1Date']` };

    @locator private lblReturnFrom: ILocator = { xpath: `//table[@id='tblLeg2APs']//td[1]` };
    @locator private lblReturnTo: ILocator = { xpath: `//table[@id='tblLeg2APs']//td[2]` };
    @locator private lblReturnDate: ILocator = { xpath: `//table[@id='tblLeg2Info']//span[@id='Leg2Date']` };

    @locator private lblAdultNo: ILocator = { xpath: `//table[@id='tblPaxCountsInfo']//td[1]` };

    /*=======================FUNCTIONS=======================*/
    public async checkTicketInfo(ticket: Ticket): Promise<void> {
        let actual;
        await checkTextContain(this.lblDepartFrom, ticket.departFrom);

        await checkTextContain(this.lblDepartTo, ticket.departTo);

        actual = await utilityHelper.getInnerTextProp(this.lblDepartDate);
        await gondola.checkEqual(actual, ticket.departDate);

        await checkTextContain(this.lblReturnFrom, ticket.returnFrom);

        await checkTextContain(this.lblReturnTo, ticket.returnTo);

        actual = await utilityHelper.getInnerTextProp(this.lblReturnDate);
        await gondola.checkEqual(actual, ticket.returnDate);

        await checkTextContain(this.lblAdultNo, ticket.adults);
    }
}

export default new BookingSummaryPage();
