import { gondola } from "gondolajs";
import { BookingSummaryPage } from "./bookingSummary";

export class PassengerInfoPage extends BookingSummaryPage {

    /*=======================LOCATORS=======================*/
    private lblPageTitle: ILocator = { xpath: `//div[@id='contentwsb']/h1[1]` };

    /*=======================FUNCTIONS=======================*/
    public async checkPageIsDisplayed():Promise<void> {
        return gondola.checkControlExist(this.lblPageTitle);
    }
}

export default new PassengerInfoPage();