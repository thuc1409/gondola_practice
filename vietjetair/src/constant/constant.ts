export const AUT_URL = `https://www.vietjetair.com/Sites/Web/en-US/Home`;

// Store all waiting variables
export const SHORT_CONTROL_WAIT = 5;
export const DYNAMIC_WAIT = 0.5;
export const MED_WAIT = 30;

export const MONTHS = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

// REGEX
export const REGEX_REMOVE_LINE_BREAK_AND_BLANK = /(^\s*(?!.+)\n+)|(\n+\s+(?!.+)$)/g;
export const REGEX_EXPECT_NUMBER = /[^0-9.]/g;