import { isArray } from "util";

/**---------------------------------------------------------
 * @method isNullOrEmpty
 * @purpose Check object is Null Or Empty
 * @param object object which we need to check
 * @returns true / false
 *---------------------------------------------------------*/
export function isNullOrEmpty(object: any): boolean {
    if (object == null || object === "") {
        return true;
    }
    else if (Object.keys(object).length === 0)
        return true;
    else if (isArray(object)) {
        if (object.length === 0)
            return true;
        else 
            return false;
    }
    else return false;
}

/*-------------------------------------------------------------------
| Function: cloneObject
| Purpose:  create a new object from an existing object
| Parameters:  
|     obj: origin object need to be clone
| Returns: clone object
*-------------------------------------------------------------------*/
export function cloneObject(obj: { [x: string]: any; constructor: new () => any; hasOwnProperty: (arg0: string) => any; }): object {
    var _out = new obj.constructor;

    var getType = function (n: any) {
        return Object.prototype.toString.call(n).slice(8, -1);
    }

    for (var _key in obj) {
        if (obj.hasOwnProperty(_key)) {
            _out[_key] = getType(obj[_key]) === 'Object' || getType(obj[_key]) === 'Array' ? cloneObject(obj[_key]) : obj[_key];
        }
    }

    return _out;
}