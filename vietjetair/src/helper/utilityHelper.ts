import Constant = require("../constant/constant");
import { gondola } from "gondolajs";

export function getDate(value: string): Date {
    let date = new Date();

    if (value.indexOf("|", 0) != -1) {
        let valuePart = value.split("|")[0];
        if (value.split("|")[1].toUpperCase() == "DAY") {
            if (valuePart.includes("-", 0)) {
                date.setDate(date.getDate() - parseInt(valuePart))
            }
            else {
                date.setDate(date.getDate() + parseInt(valuePart))
            }
        }
        else if (value.split("|")[1].toUpperCase() == "MONTH") {
            if (valuePart.includes("-", 0)) {
                date.setMonth(date.getMonth() - parseInt(valuePart));
            }
            else {
                date.setMonth(date.getMonth() + parseInt(valuePart));
            }
        }
        else if (value.toUpperCase() == "<TODAY>") {
            // Implement later
        }
    }
    else {
        date = new Date(value)
    }
    return date;
}
export function formatDateTime(value: string, format: string = "MM/DD/YY"): string {
    let returnDate: string = "";
    let date = new Date(getDate(value));
    let y = date.getFullYear();
    let m = date.getMonth() + 1;
    let d = date.getDate();
    switch (format) {
        case "DD/MM/YY":
            returnDate = `${("0" + d).slice(-2)}/${("0" + m).slice(-2)}/${y}`
            break;
        case "MM/DD/YY":
            returnDate = `${("0" + m).slice(-2)}/${("0" + d).slice(-2)}/${y}`
            break;
        default:
            break;
    }

    return returnDate
}
export async function selectDateOfWidget(value: string): Promise<void> {
    let date = new Date(getDate(value));
    let targetYear = date.getFullYear();
    let targetMonth = Constant.MONTHS[date.getMonth()];
    let targetDay = date.getDate();

    let curentYear = await gondola.getControlProperty("//div[@id='ui-datepicker-div']//span[@class='ui-datepicker-year']", "innerText");

    // Select Year
    while (curentYear != targetYear) {
        if (curentYear > targetYear) {
            await gondola.click("//a[@class='ui-datepicker-prev ui-corner-all']");
        }
        else {
            await gondola.click("//a[@class='ui-datepicker-next ui-corner-all']");
        }
    }

    // Select Month
    await gondola.select("//select[@class='ui-datepicker-month']", targetMonth);

    // Select Day
    await gondola.click(`//table[@class='ui-datepicker-calendar']/tbody/tr/td/a[text()='${targetDay}']`);

    // Wait for Widget disapear
    await gondola.waitForDisappear("//div[id='ui-datepicker-div']")
}

export function getCurrentMonth(digitNumber: number = 2): string {
    let date = new Date()
    return ("0" + (date.getMonth() + 1)).slice(-digitNumber)
}

export function getCurrentYear(): string {
    let date = new Date()
    return date.getFullYear().toString();
}

export async function checkTextContain(element: string | ILocator, value: string): Promise<void> {
    let text = await gondola.getControlProperty(element, "innerText");

    if (text.indexOf(value, 0) != -1) {
        gondola.checkEqual(true, true, `${text} has contains ${value}`);
    }
    else {
        gondola.checkEqual(true, false, `${text} has not contains ${value}`);
    }
}

export async function getInnerTextProp(locator: ILocator | string):Promise<any> {
    return await gondola.getControlProperty(locator, "innerText");
}