export class Ticket {

    private _flightType: string = '';

    private _departFrom: string = '';
    private _stationCodeFrom: string = '';
    private _departTo: string = '';
    private _departDate: string = '';
    private _departFare: string = '';

    private _returnFrom: string = '';
    private _stationCodeTo: string = '';
    private _returnTo: string = '';
    private _returnDate: string = '';
    private _returnFare: string = '';

    private _adults: string = '';
    private _currency: string = '';

    get flightType(): string {
        return this._flightType
    }
    set flightType(value: string) {
        this._flightType = value;
    }

    get departFrom(): string {
        return this._departFrom;
    }

    set departFrom(value: string) {
        this._departFrom = value;
    }

    get stationCodeFrom(): string {
        return this._stationCodeFrom;
    }

    set stationCodeFrom(value: string) {
        this._stationCodeFrom = value;
    }

    get departTo(): string {
        return this._departTo;
    }

    set departTo(value: string) {
        this._departTo = value;
    }

    get departDate(): string {
        return this._departDate;
    }

    set departDate(value: string) {
        this._departDate = value;
    }

    get departFare(): string {
        return this._departFare;
    }

    set departFare(value: string) {
        this._departFare = value;
    }

    get returnFrom(): string {
        return this._returnFrom;
    }

    set returnFrom(value: string) {
        this._returnFrom = value;
    }

    get stationCodeTo(): string {
        return this._stationCodeTo;
    }

    set stationCodeTo(value: string) {
        this._stationCodeTo = value;
    }

    get returnTo(): string {
        return this._returnTo;
    }

    set returnTo(value: string) {
        this._returnTo = value;
    }

    get returnDate(): string {
        return this._returnDate;
    }

    set returnDate(value: string) {
        this._returnDate = value;
    }

    get returnFare(): string {
        return this._returnFare;
    }

    set returnFare(value: string) {
        this._returnFare = value;
    }

    get adults(): string {
        return this._adults;
    }

    set adults(value: string) {
        this._adults = value;
    }

    get currency(): string {
        return this._currency;
    }

    set currency(value: string) {
        this._currency = value;
    }
}