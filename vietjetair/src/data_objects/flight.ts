export class Flight {
    private _flightDate: string;
    private _depart: string;
    private _arrive: string;
    private _flightDetail: string;
    private _promo: string;
    private _eco: string;
    private _skyboss: string;

    constructor(flightDate: string = "", depart: string = "", arrive: string = "",
        flightDetail: string = "", promo: string = "", eco: string = "",
        skyboss: string = "") {
        this._flightDate = flightDate;
        this._depart = depart;
        this._arrive = arrive;
        this._flightDetail = flightDetail;
        this._promo = promo;
        this._eco = eco;
        this._skyboss = skyboss;
    }

    /**
     * Getter flightDate
     * @return {string}
     */
	public get flightDate(): string {
		return this._flightDate;
	}

    /**
     * Getter depart
     * @return {string}
     */
	public get depart(): string {
		return this._depart;
	}

    /**
     * Getter arrive
     * @return {string}
     */
	public get arrive(): string {
		return this._arrive;
	}

    /**
     * Getter flightDetail
     * @return {string}
     */
	public get flightDetail(): string {
		return this._flightDetail;
	}

    /**
     * Getter promo
     * @return {string}
     */
	public get promo(): string {
		return this._promo;
	}

    /**
     * Getter eco
     * @return {string}
     */
	public get eco(): string {
		return this._eco;
	}

    /**
     * Getter skyboss
     * @return {string}
     */
	public get skyboss(): string {
		return this._skyboss;
	}

    /**
     * Setter flightDate
     * @param {string} value
     */
	public set flightDate(value: string) {
		this._flightDate = value;
	}

    /**
     * Setter depart
     * @param {string} value
     */
	public set depart(value: string) {
		this._depart = value;
	}

    /**
     * Setter arrive
     * @param {string} value
     */
	public set arrive(value: string) {
		this._arrive = value;
	}

    /**
     * Setter flightDetail
     * @param {string} value
     */
	public set flightDetail(value: string) {
		this._flightDetail = value;
	}

    /**
     * Setter promo
     * @param {string} value
     */
	public set promo(value: string) {
		this._promo = value;
	}

    /**
     * Setter eco
     * @param {string} value
     */
	public set eco(value: string) {
		this._eco = value;
	}

    /**
     * Setter skyboss
     * @param {string} value
     */
	public set skyboss(value: string) {
		this._skyboss = value;
	}
}