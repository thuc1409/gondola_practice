export class Search {

    private _flightType: string = '';
    private _origin: string = '';
    private _departDate: string = '';
    private _destination: string = '';
    private _returnDate: string = '';
    private _currency: string = '';
    private _adults: string;
    private _findLowest: boolean = false;


    constructor(flightType: string = "return", findLowest: boolean = false, origin: string = "",
        departDate: string = "", destination: string = "", returnDate: string = "", currency: string = "",
        adults: string = "") {
        this._flightType = flightType;
        this._origin = origin;
        this._departDate = departDate;
        this._destination = destination;
        this._returnDate = returnDate;
        this._currency = currency;
        this._adults = adults;
        this._findLowest = findLowest;
    }

    get flightType(): string {
        return this._flightType
    }
    set flightType(value: string) {
        this._flightType = value;
    }

    get origin(): string {
        return this._origin
    }
    set origin(value: string) {
        this._origin = value;
    }

    get departDate(): string {
        return this._departDate
    }
    set departDate(value: string) {
        this._departDate = value;
    }

    get destination(): string {
        return this._destination
    }
    set destination(value: string) {
        this._destination = value;
    }

    get returnDate(): string {
        return this._returnDate
    }
    set returnDate(value: string) {
        this._returnDate = value;
    }

    get currency(): string {
        return this._currency
    }
    set currency(value: string) {
        this._currency = value;
    }

    get findLowest(): boolean {
        return this._findLowest
    }
    set findLowest(value: boolean) {
        this._findLowest = value;
    }

    get adults(): string {
        return this._adults
    }
    set adults(value: string) {
        this._adults = value;
    }
}