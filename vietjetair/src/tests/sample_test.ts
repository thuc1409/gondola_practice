import { TestCase, TestModule } from "gondolajs";
import homePage from "../pages/homePage";
import selectTravelOptionsPage from "../pages/selectTravelOptionsPage";
import passengerInfoPage from "../pages/passengerInfoPage";
import selectFarePage from "../pages/selectFarePage";
import { Search } from "../data_objects/search";

TestModule("VietjetAir");

TestCase("TC01: Search and choose tickets on a specific day successfully", async () => {
    let searchObj = new Search("Return", false,
        "Ho Chi Minh (SGN)", "+5|day",
        "Ha Noi (HAN)", "+7|day",
        "VND", "2");

    await homePage.navigateTo();
    await homePage.searchFlights(searchObj);

    await selectTravelOptionsPage.checkPageIsDisplayed();
    await selectTravelOptionsPage.checkFlightsInformation(searchObj);

    let ticketObj = await selectTravelOptionsPage.selectCheapestFlights(searchObj);
    await selectTravelOptionsPage.clickContinue();

    await passengerInfoPage.checkPageIsDisplayed();
    await passengerInfoPage.checkTicketInfo(ticketObj);
});

TestCase("TC02: Search and choose cheapest tickets on next 3 months successfully", async () => {
    let searchObj = new Search();
    searchObj.flightType = "Return";
    searchObj.origin = "Ho Chi Minh (SGN)";
    searchObj.destination = "Ha Noi (HAN)";
    searchObj.findLowest = true;
    searchObj.adults = "2";

    await homePage.navigateTo();
    await homePage.searchFlights(searchObj);
    await selectFarePage.checkPageIsDisplayed()
    await selectFarePage.selectCheapestTicketInMonths(3);
    await selectFarePage.clickContinue();

    let ticketObj = await selectTravelOptionsPage.selectCheapestFlights(searchObj);
    await selectTravelOptionsPage.clickContinue();

    await passengerInfoPage.checkPageIsDisplayed();
    await passengerInfoPage.checkTicketInfo(ticketObj);
});